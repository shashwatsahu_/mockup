const COLORS = {
    white: '#fff',
    black: '#000',
    headerBlue: '#4281f5',
    gray: '#bfbfbf',
  }

export default COLORS;
