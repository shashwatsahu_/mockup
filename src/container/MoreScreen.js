import React from 'react';
import { View, Text } from 'react-native';

class MoreScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>FeaturedScreen!</Text>
        </View>
      );
    }
  }

  export { MoreScreen };
  