import React, {Component, useState} from 'react';
import {
  View,
  Text,
  Picker,
  StyleSheet,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {Header} from '../common';
import COLORS from '../assets/colors/colors';
import {data} from '../data';

class CouponScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numColumns: 2,
      modalVisible: false,
      selectedText: 'Pounds Captured'
    };
    this.bottomContent = this.bottomContent.bind(this);
    this.mainContent = this.mainContent.bind(this);
    this.modalContent = this.modalContent.bind(this);
    this.setModalVisible = this.setModalVisible.bind(this);
    this.setPickerText = this.setPickerText.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  setPickerText(value){
    console.log('value:', value);
    this.setState({
      selectedText: value,
    })
  }

  bottomContent() {
    return (
      <View style={{flex: 1, marginLeft: 8, marginRight: 8}}>
        <FlatList
          data={data}
          key={this.state.numColumns}
          numColumns={this.state.numColumns}
          renderItem={({item}) => (
            <Item
              title={item.firstData}
              subtitle={item.secondData}
              src={item.src}
            />
          )}
          keyExtractor={item => item.id}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }

   modalData = [{
    id: '1',
    value: 'Pounds Captured',
  },
  {
    id: '2',
    value: 'Option 2',
  },
  {
    id: '3',
    value: 'Option 3',
  }
];



  modalContent() {
    return(
      <View>
      <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}>

          <View style={styles.dialogParent}>
            <View style={styles.dialog}>
              <FlatList 
                  data={this.modalData}
                  renderItem={({item}) => (
                    <ModalListItem
                      value={item.value}
                      setModalVisible={this.setModalVisible}
                      selectedText={this.setPickerText}
                     />
                )}
                keyExtractor={item => item.id}
                showsVerticalScrollIndicator={false}
              />
                        
                
            </View>
          </View>
      </Modal>
      </View>     
    )
  }

  mainContent() {
    return (
      <View style={styles.secondContainer}>
          {this.modalContent()}
        <TouchableOpacity style={styles.pickerContainer}
        onPress={() => this.setModalVisible(true)}
        >
          {/* <Picker
            style={styles.picker}
            selectedValue={'Pounds Captured'}
            // style={{height: 50, width: 100}}
            onValueChange={(itemValue, itemIndex) => itemValue}>
            <Picker.Item label="Pounds Captured" value="Pounds Captured" />
            <Picker.Item label="Dollar" value="Dollar Captured" />
          </Picker> */}

          <Text style={styles.selectedText}>
           {this.state.selectedText}
          </Text>
        
          <Image
            style={styles.dropdownArrow}
            source={require('../assets/dropdown_arrow.png')}
          />

        </TouchableOpacity>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => this.setState({numColumns: 3})}>
            <Image
              style={[styles.secondContainerImage, this.state.numColumns === 3 ? {tintColor: COLORS.headerBlue} : {tintColor : COLORS.gray}]}
              source={require('../assets/ic_tile.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.setState({numColumns: 2})}>
            <Image
              style={[styles.secondContainerImage, this.state.numColumns === 2 ? {tintColor: COLORS.headerBlue} : {tintColor : COLORS.gray}]}
              source={require('../assets/ic_tileViewActiveBlue.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.setState({numColumns: 1})}>
            <Image
              style={[styles.secondContainerImage, this.state.numColumns === 1 ? {tintColor: COLORS.headerBlue} : {tintColor : COLORS.gray}]}
              source={require('../assets/ic_listViewInactive.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Header name="SHOP" isSearch={true} />
        {this.mainContent()}
        {this.bottomContent()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, flexDirection: 'column'},
  secondContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  pickerContainer: {
    height: 40,
    width: Dimensions.get('window').width / 2,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: COLORS.gray,
    borderRadius: 5,
    marginLeft: 12,
    marginTop: 8,
    marginBottom: 8,
    marginRight: 12,
  },
  picker: {
    color: 'black',
    borderColor: 'black',
    borderWidth: 2,
  },
  selectedText: {
      fontSize: 16,
      marginLeft: 12,
  },
  dropdownArrow: {
    width: 16, 
    height: 16,
    marginLeft: 4, 
    marginRight: 8 
  },
  secondContainerImage: {
    width: 25,
    height: 25,
    marginLeft: 12,
    marginRight: 12,
  },
  item: {
    // width:Dimensions.get('window').width/2.2,
    backgroundColor: 'white',
    borderColor: COLORS.gray,
    borderWidth: 1,
    borderRadius: 4,
    margin: 4,
    flex: 1,
    flexDirection: 'column',
  },
  title: {
    fontSize: 16,
    alignSelf: 'center',
  },
  subTitle: {
    color: 'green',
    fontSize: 18,
    alignContent: 'center',
  },
  heart: {
    width: 30,
    height: 30,
    margin: 12,
  },
  dollar: {
    width: 30,
    height: 30,
    alignSelf: 'flex-end',
    marginBottom: 8,
    marginRight: 8,
  },
  dialog: {
    backgroundColor: 'white',
    width: '70%',
    minHeight: 70,
    borderRadius:5, 
}, 
dialogParent:{
    flexDirection: 'column', 
    justifyContent: 'center', 
    alignItems: 'center', 
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
}, 
modalListItem: {
  margin: 8,
  alignItems: 'center',
  justifyContent: 'center',
  paddingVertical: 8,
}
});

function Item({title, src, subtitle}) {
  const [ selected, setSelected ] = useState(false);
  return (
    <View style={styles.item}>
      <TouchableOpacity
      onPress={() => setSelected(!selected)}
      style={{width: 50, height: 50}}>
        <Image
          style={[styles.heart, !selected && { tintColor: 'grey' } ]}
          resizeMode="contain"
          source={require('../assets/ic_Favorite.png')}
        />
      </TouchableOpacity>
      <Image
        resizeMode="contain"
        style={{
          width: 100,
          height: 80,
          alignSelf: 'center',
        }}
        source={src}
      />
      <Text style={styles.title}>{title}</Text>

      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <View style={{ width: 18, height: 10 }} />
        <Text style={styles.subTitle}>{subtitle}</Text>
        <Image
          style={styles.dollar}
          source={require('../assets/ic_couponBlue.png')}
        />
      </View>
    </View>
  );
}

function ModalListItem({value, setModalVisible, selectedText}) {
  return(
    <TouchableOpacity 
    style={styles.modalListItem}
    onPress={() => {
      selectedText(value);
      setModalVisible(false);
      }}>
    <Text>{value}</Text>
    </TouchableOpacity>
  )
}

export {CouponScreen};
