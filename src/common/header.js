import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions, SafeAreaView, StatusBar} from 'react-native';
import COLORS from '../assets/colors/colors';
import {userImage} from '../assets/index';

const Header = ({name, isSearch}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={COLORS.headerBlue} barStyle="light-content" />
      <View style={styles.firstRow}>
        <View style={{ flex: 1.5, paddingHorizontal: 10 }}>
        <View style={styles.leftPlaceHolder}>
          <Text style={styles.leftText}>589,497 LBS CAPTURED</Text>
        </View>
        </View>
        <View style={{ flex: 1, alignItems: 'flex-start' }}>
          <Text style={styles.headerText}>{name}</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center' }}>
          <Image
            source={require('../assets/ic_user.png')}
            resizeMode="contain"
            style={styles.rightImage}
          />
        </View>
      </View>
      {isSearch ? (
        <View style={styles.secondRow}>
          <View style={styles.searchContainer}>
            <Image
              source={require('../assets/search.png')}
              style={styles.searchIcon}
            />
            <Text style={styles.searchText}>Search</Text>
          </View>
        </View>
      ) : (
        <View />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: COLORS.headerBlue,
  },
  searchIcon: {
    tintColor: COLORS.gray,
    width: 20,
    height: 20,
  },
  firstRow: {
    height: 60,
    backgroundColor: COLORS.headerBlue,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  secondRow: {
    height: 60,
    backgroundColor: COLORS.gray,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: COLORS.white,
    fontSize: 18,
  },
  leftPlaceHolder: {
    borderColor: COLORS.white,
    borderWidth: 1.5,
    borderRadius: 4,
    backgroundColor: 'rgba(255,255,255, 0.3)',
  },
  leftText: {
    color: COLORS.white,
    fontSize: 12,
    margin: 4,
  },
  rightImage: {
    flex: 1,
    alignSelf: 'flex-end',
    width: 30,
    height: 30,
  },
  searchContainer: {
    backgroundColor: 'white',
    height: 40,
    borderRadius: 4,
    width: Dimensions.get('window').width - 16,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  searchText: {
    color: COLORS.gray,
    fontSize: 20,
    marginLeft: 12,
  },
});

export {Header};
