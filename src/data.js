export const data = [
    {
        id: '1',
        firstData: '8 lbs. /$',
        secondData: '10 LBS.',
        src: require('./assets/cyclegear.png'),
    },
    {
        id: '2',
        firstData: '10 lbs. /$',
        secondData: '30 LBS.',
        src: require('./assets/allPosters.png'),
    },
    {
        id: '3',
        firstData: '80 lbs. /$',
        secondData: '112 LBS.',
        src: require('./assets/autobarn.png'),
    },
    {
        id: '4',
        firstData: '500 lbs. /booking',
        secondData: '11 LBS.',
        src: require('./assets/booking.png'),
    },
    {
        id: '5',
        firstData: '80-10 lbs. /$2',
        secondData: '43 LBS.',
        src: require('./assets/chemicalguys.png'),
    },
    {
        id: '6',
        firstData: '80 lbs. /$10',
        secondData: '200 LBS.',
        src: require('./assets/oscaro.png'),
    },
];