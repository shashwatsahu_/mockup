import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import {
        CouponScreen,
        FeaturedScreen,
        MoreScreen,
        FavoriteScreen,
        CategoriesScreen,
      } from './src/container';
import { createAppContainer } from 'react-navigation';
import {strings} from './src/assets/constants';
import { createBottomTabNavigator } from 'react-navigation-tabs';

class App extends Component {
  render() {
    return (
     <AppContainer />
    )
  }
}

const routeConfiguration = {
  Favorites: {
    screen: FavoriteScreen,
    navigationOptions: {
      tabBarLabel: strings.Favorites,
      tabBarIcon: ({tintColor}) => (
        <Image 
        tintColor={tintColor}
        style={styles.bottomImage}
        resizeMode='contain'
        source={require('./src/assets/ic_navFavorites.png')}
     />  
      )
    }
  },
  Feature: {
    screen: FeaturedScreen,
    navigationOptions: {
      tabBarLabel: strings.Featured,
      tabBarIcon: ({tintColor}) => (
        <Image
         tintColor={tintColor}
         style={styles.bottomImage}
         resizeMode='contain'
         source={require('./src/assets/ic_navFeatured.png')}
        />
      )
    }
   }
   ,Coupon: {
      screen: CouponScreen,
      navigationOptions: {
        tabBarLabel: strings.Coupons,
        tabBarIcon:({tintColor}) => (
          <Image 
            tintColor={tintColor}
            style={styles.bottomImage}
            resizeMode='contain'
            source={require('./src/assets/ic_couponBlue.png')}
         />  
      )  
      },
    },
    Categories: {
      screen: CategoriesScreen,
      navigationOptions: {
        tabBarLabel: strings.Categories,
        tabBarIcon: ({tintColor}) => (
          <Image
           tintColor={tintColor}
           style={styles.bottomImage}
           resizeMode='contain'
           source={require('./src/assets/ic_navCategories.png')}
          />
        )
      }
     },
     More: {
       screen: MoreScreen,
       navigationOptions: {
         tabBarLabel: strings.More,
         tabBarIcon: ({tintColor}) => (
           <Image
              tintColor={tintColor}
              style={styles.bottomImage}
              resizeMode='contain'
              source={require('./src/assets/ic_navMore.png')}
           />
         )
       }
     }
  };

  const tabBarConfiguration = {
    tabBarOptions: {
    style: {
      backgroundColor: 'black',
    }, 
  },
  initialRouteName: 'Coupon'};

const TabNavigator = createBottomTabNavigator(routeConfiguration, tabBarConfiguration);

const AppContainer = createAppContainer(TabNavigator);

export default App;

const styles = StyleSheet.create({
  bottomImage:{width: 20, height: 20}
})
